import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Options } from '@angular-slider/ngx-slider';
import { Router } from '@angular/router';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CommonDataServiceService } from '../common-data-service.service';
import { ChangeDetectorRef } from '@angular/core'

export interface Hobby {
  name: string;
}

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})


export class RegistrationFormComponent implements OnInit {
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  hobbies: Hobby[] = [];
  src = "https://anju.blob.core.windows.net/sushin/empty";

  isFirstNameValid = true;
  isEmailValid = true;
  isTelNoValid = true;
  showAddressesLine = false;
  isStateValid = true;


  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.hobbies.push({ name: value });
    }
    event.chipInput!.clear();
  }

  remove(hobby: Hobby): void {
    const index = this.hobbies.indexOf(hobby);
    if (index >= 0) {
      this.hobbies.splice(index, 1);
    }
  }

  value: number = 25;
  options: Options = {
    floor: 13,
    ceil: 50,
    combineLabels: (minValue: string, maxValue: string): string => {
      return 'from ' + minValue + ' up to ' + maxValue;
    }
  }

  stateList = [
    { id: '1', name: 'Kerala' },
    { id: '2', name: 'Tamil Nadu' },
    { id: '3', name: 'Assam' },
  ];

  addressList = [
    { addressType: 'Home' },
    { addressType: 'Office' }
  ];


  constructor(
    private route: Router,
    private commonDataServiceService: CommonDataServiceService,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.contactForm.get('hobbies')?.setValue(null)
  }


  contactForm = new FormGroup({
    firstname: new FormControl("", [Validators.required, Validators.maxLength(20), Validators.pattern('^[a-zA-Z ]*$')]),
    lastname: new FormControl(),
    email: new FormControl("", [Validators.email, Validators.required]),
    tel: new FormControl("", [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10)]),
    state: new FormControl("", [Validators.required]),
    addressType: new FormControl("", [Validators.required]),
    address1: new FormControl(),
    address2: new FormControl(),
    hobbies: new FormControl(),
    age: new FormControl(),
    subscribed: new FormControl()
  })

  onSubmit() {
    localStorage.setItem("datas", JSON.stringify(this.contactForm.value));
    this.route.navigate(['/view-details']);
    this.contactForm.get('hobbies')?.setValue(this.hobbies)
    localStorage.setItem("datas", JSON.stringify(this.contactForm.value));
    this.route.navigate(['/view-details']);
  }

  addressTypeChange() {
    this.showAddressesLine = true;
  }

  nameValidation() {
    if (this.contactForm.get('firstname')?.valid) {
      this.isFirstNameValid = true
    } else {
      this.isFirstNameValid = false;
    }
  }

  getIsEmailValid() {
    if (this.contactForm.get('email')?.valid) {
      this.isEmailValid = true;
    } else {
      this.isEmailValid = false;
    }
  }

  getIsTelNoValid() {
    if (this.contactForm.get('tel')?.valid) {
      this.isTelNoValid = true;
    } else {
      this.isTelNoValid = false;
    }
  }
  getIsStateValid() {
    if (this.contactForm.get('state')?.valid) {
      this.isStateValid = true;
    } else {
      this.isStateValid = false;
    }
  }

  setEmptyProfilePic() {
    this.src = "https://anju.blob.core.windows.net/sushin/empty";
  }

  uploadPhoto(event: any) {
    this.commonDataServiceService.uploadToAzure(event).then((resl => {
      this.src = "https://anju.blob.core.windows.net/sushin/profilePic?sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacuptfx&se=2100-10-15T10:40:41Z&st=2021-10-15T02:40:41Z&spr=https,http&sig=Toe5%2BEx%2FxsXVaMcsezMMTktf%2BjVi75hoy4yQZOIZr78%3D";
      this.ref.detectChanges();
    }));
    this.ref.detectChanges();
  }


}

