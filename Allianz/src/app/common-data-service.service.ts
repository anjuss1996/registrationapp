import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { uploadBrowserDataToBlockBlob,BlobURL, BlockBlobURL, ContainerURL, ServiceURL, StorageURL, AnonymousCredential, Aborter, BlobUploadCommonResponse } from '@azure/storage-blob';

@Injectable({
  providedIn: 'root'
})
export class CommonDataServiceService {

  private profileData = new BehaviorSubject<any>(null);
  public profileData$ = this.profileData.asObservable();

  constructor() { }

  setProfileData(data: any) {
    this.profileData.next(data);
  }

  uploadToAzure(event:any): Promise<BlobUploadCommonResponse> {
    const storageAccount = {
      uri: `https://anju.blob.core.windows.net`,
      sasToken:`?sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacuptfx&se=2100-10-15T10:40:41Z&st=2021-10-15T02:40:41Z&spr=https,http&sig=Toe5%2BEx%2FxsXVaMcsezMMTktf%2BjVi75hoy4yQZOIZr78%3D`,
      container:`sushin`
    }
    const pipeline = StorageURL.newPipeline(new AnonymousCredential());
    const serviceURL = new ServiceURL(storageAccount.uri + storageAccount.sasToken, pipeline);
    const containerURL = ContainerURL.fromServiceURL(serviceURL, storageAccount.container);
    const blobUrl = BlobURL.fromContainerURL(containerURL, 'profilePic');
    const blockBlobUrl = BlockBlobURL.fromBlobURL(blobUrl);
    console.log(blockBlobUrl)
    return  uploadBrowserDataToBlockBlob(Aborter.none,event.target.files[0],blockBlobUrl);
 
}

}
