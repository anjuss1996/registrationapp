import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { ViewProfileDetailsComponent } from './view-profile-details/view-profile-details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { HttpClient } from '@angular/common/http';
// import { MatRippleModule } from '@angular/material/prebuilt-themes';
import { HttpClientModule } from '@angular/common/http';
      

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    RegistrationFormComponent,
    ViewProfileDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxSliderModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatChipsModule,    
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    FormsModule
    // MatRippleModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
