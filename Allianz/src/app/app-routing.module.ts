import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { ViewProfileDetailsComponent } from './view-profile-details/view-profile-details.component';

const routes: Routes = [
  // {path: 'home', component: LandingPageComponent},
  {path: '', component: LandingPageComponent},
  {path: 'register', component: RegistrationFormComponent},
  {path: 'view-details', component: ViewProfileDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
