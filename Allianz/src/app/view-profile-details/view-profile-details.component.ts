import { Component, OnInit, } from '@angular/core';
import { CommonDataServiceService } from '../common-data-service.service';

@Component({
  selector: 'app-view-profile-details',
  templateUrl: './view-profile-details.component.html',
  styleUrls: ['./view-profile-details.component.css']
})
export class ViewProfileDetailsComponent implements OnInit {
  data: any;
  profileDetails: any;
  src = "https://anju.blob.core.windows.net/sushin/profilePic?sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacuptfx&se=2100-10-15T10:40:41Z&st=2021-10-15T02:40:41Z&spr=https,http&sig=Toe5%2BEx%2FxsXVaMcsezMMTktf%2BjVi75hoy4yQZOIZr78%3D";

  constructor(private commonDataServiceService: CommonDataServiceService) { }

  ngOnInit(): void {
    this.data = localStorage.getItem("datas");
    this.data = JSON.parse(this.data);
  }

  uploadPhoto(event: any) {
    this.commonDataServiceService.uploadToAzure(event).then((resl => {
      this.src = "https://anju.blob.core.windows.net/sushin/profilePic?sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacuptfx&se=2100-10-15T10:40:41Z&st=2021-10-15T02:40:41Z&spr=https,http&sig=Toe5%2BEx%2FxsXVaMcsezMMTktf%2BjVi75hoy4yQZOIZr78%3D";
    }));
  }

  setEmptyProfilePic() {
    this.src = "https://anju.blob.core.windows.net/sushin/empty";
  }


}
